from generic_algorithms import HillClimbing, Genetic
import pandas as pd
import numpy as np
from typing import List
import random

class TSPClassicHillClimbing(HillClimbing):
  def __init__(self, tsp: pd.Dataframe):
    super().__init__('TSP Hill Climbing', self.fitness, self.generate_subject, self.best_neighbour)
    self.tsp = tsp
    
  def generate_subject(self):
    cities = list(self.tsp.keys())
    subject = []

    city = cities[0]
    subject.append(city)
    cities.remove(city)

    for _ in range(0,len(cities)):
      city = random.choice(cities)
      subject.append(city)
      cities.remove(city)

    return subject
  
  def fitness(self, subject: List[str]):
    fitness = 0

    for i in range(len(subject)):
      k = (i+1) % len(subject)
      city_a = subject[i]
      city_b = subject[k]

      fitness += self.tsp.loc[city_a, city_b]

    return fitness
  
  def _generate_neighbour(self, subject: List[str]):
    for i in range(1, len(subject)):
      for j in range(i + 1, len(subject)):
        neighbour = subject.copy()
        neighbour[i] = subject[j]
        neighbour[j] = subject[i]

        yield(neighbour)
        
  def best_neighbour(self, subject: List[str]):
    best_fitness = self.fitness(subject)  
    best_neighbour = subject

    for neighbour in self._generate_neighbour(subject):
      actual_fitness = self.fitness(neighbour)
      if actual_fitness < best_fitness:
        best_fitness = actual_fitness
        best_neighbour = neighbour

    return best_neighbour, best_fitness


class EightQueensGenetic(Genetic):
  def __init__(self):
    super().__init__('Eight Queens Genetic Algorithm', self.generate_weights, 
                      self.generate_population, self.select,
                      self.crossover, self.mutate, self.best_individual)
    
  def generate_subject(self):
    return np.random.randint(low=1, high=7, size=8)

  def generate_population(self, population_size: int):
    return [self.generate_subject() for _ in range(population_size)]
  
  def best_individual(self, population: int):
    return {"subject": population[self.fitness_fn(population).index(min(self.fitness_fn(population)))],
            "fitness": min(self.fitness_fn(population))}

  def _count_line_attacks(self, subject: np.array):
    attacks = 0
    N = len(subject)
    for col1 in range(N):
      lin1 = subject[col1]
      for col2 in range(col1+1, N):
        lin2 = subject[col2]
        if lin1==lin2:
          attacks +=1

    return attacks
    
  def _count_diagonal_attacks(self, subject: np.array):
    attacks = 0
    N = len(subject)

    for col1 in range(N):
      lin1 = subject[col1]
      for col2 in range(col1+1, N):
        lin2 = subject[col2]

        d1 = lin1-col1
        d2 = lin2-col2

        s1 = lin1+col1
        s2 = lin2+col2

        if d1==d2 or s1==s2:
          attacks +=1
    
    return attacks

  def _count_attacks(self, subject: np.array):
    return self._count_line_attacks(subject) + self._count_diagonal_attacks(subject)

  def generate_weights(self, population: List[np.array]):
    return [self._count_attacks(subject) for subject in population]

  def select(self, population: List[np.array]):
    chosen_one = random.choice(population)
    chosen_two = random.choice(population)
    
    attacks_one = self._count_attacks(chosen_one)
    attacks_two = self._count_attacks(chosen_two)
    
    return chosen_one if attacks_one <= attacks_two else chosen_two
    
  def crossover(self, parent_one: np.array, parent_two: np.array):
    n = len(parent_one)
    split_point = np.random.randint(1, n-1)
    return np.concatenate((parent_one[:split_point], parent_two[split_point:])), np.concatenate((parent_two[:split_point], parent_one[split_point:]))


  def mutate(self, subject: np.array, p_mutation: float = 0.30):
    mutated = subject.copy()
    
    n = len(subject)
    p = np.random.rand()
    
    if p < p_mutation:
      col = np.random.randint(0, n)
      line = np.random.randint(1, n+1)
      
      mutated[col] = line
      
    return mutated

