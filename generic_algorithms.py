from typing import Callable
from tqdm import tqdm
import numpy as np

class Base:
  def __init__(self, name: str):
    self.name = name

  def run(self):
    raise NotImplementedError('You must implement the run method')

  def __str__(self):
    return self.name
  
class Genetic(BaseAlgorithm):
  def __init__(self, name: str, fitness_fn: Callable, population_fn: Callable, select_fn: Callable,
                crossover_fn: Callable, mutate_fn: Callable, best_individual_fn: Callable):
    super().__init__(name)
    self.fitness_fn = fitness_fn
    self.population_fn = population_fn
    self.select_fn = select_fn
    self.crossover_fn = crossover_fn
    self.mutate_fn = mutate_fn
    self.best_individual_fn = best_individual_fn 
  
  def run(self, num_generations: int, population_size: int):
    population = self.population_fn(population_size)
    
    for _ in tqdm(range(num_generations), desc = 'Running Generations'):
      new_population = []
      for _ in range(0, population_size):
        parent_one = self.select_fn(population)
        parent_two = self.select_fn(population)

        child_one, child_two = self.crossover_fn(parent_one, parent_two)
        child_one, child_two = self.mutate_fn(child_one), self.mutate_fn(child_two)

        new_population.append(child_one)
        new_population.append(child_two)

      population = new_population
      
    best_individual = self.best_individual_fn(population)
    print(f'Best Individual: {best_individual["subject"]}')
    print(f"Fitness: {best_individual['fitness']}")
      
    return best_individual
  
class HillClimbing(BaseAlgorithm):
  def __init__(self, name: str, fitness_fn: Callable, generate_subject_fn: Callable, best_neigbour_fn: Callable):
    super().__init__(name)
    self.fitness_fn = fitness_fn
    self.generate_subject_fn = generate_subject_fn
    self.best_neighbour_fn = best_neigbour_fn

  def run(self):
    best_subject = self.generate_subject_fn()
    best_fitness = self.fitness_fn(best_subject)
    
    progress_bar = tqdm()
    while True:
      neighbour, neighbour_fitness = self.best_neighbour_fn(best_subject)
      
      if neighbour_fitness < best_fitness:
        best_subject = neighbour
        best_fitness = neighbour_fitness
      else:
        progress_bar.close()
        return best_subject, best_fitness
      progress_bar.update(1)

class SimulateAnnealing(BaseAlgorithm):
  def __init__(self, name: str, fitness_fn: Callable, generate_subject_fn: Callable, generate_neighbor_fn: Callable,
                acceptance_probability: Callable, temperature: float, cooling_rate: float):
    super().__init__(name)
    self.fitness_fn = fitness_fn
    self.generate_subject_fn = generate_subject_fn
    self.best_neighbour_fn = best_neigbour_fn
    self.temperature = temperature
    self.cooling_rate = cooling_rate

